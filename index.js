// Bitcoin RPC: Testnet => 18332 Mainnet 8332
// Crown RPC: Testnet => 19341 Mainnet 9341

// Option1. Use rpc client
var Client = require("rpc-client");

client = new Client({host:"localhost",port:"18332", protocol:"http"});
client.setBasicAuth("test", "test");
client.call("getblockcount", null, function(err, result){
  console.log('result', result)
})


// Option2. Use request 
var request = require('request');

var options = {
  url: "http://localhost:18332",
  method: "post",
  headers: {
    "content-type": "text/plain"
  },
  auth: {
      user: "test",
      pass: "test"
  },
  body: JSON.stringify( {"jsonrpc": "1.0", "id": "curltest", "method": "getblockcount", "params": [] })
};

request(options, (error, response, body) => {
  if (error) {
      console.error('An error has occurred: ', error);
  } else {
      body = JSON.parse(body);
      console.log('Post successful: response: ', body);

  }
});